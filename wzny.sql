create database wzny default charset=utf8;

use wzny；

/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : wzny

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2021-09-13 20:34:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for game
-- ----------------------------
DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int(16) NOT NULL COMMENT '游戏id',
  `pid` int(16) NOT NULL COMMENT '玩家id',
  `playtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '玩游戏的时间',
  `score` int(12) DEFAULT NULL COMMENT '分数',
  `loginname` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game
-- ----------------------------

-- ----------------------------
-- Table structure for gamer
-- ----------------------------
DROP TABLE IF EXISTS `gamer`;
CREATE TABLE `gamer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '玩家id',
  `loginname` varchar(60) NOT NULL COMMENT '玩家名称',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `sex` varchar(6) DEFAULT NULL COMMENT '性别',
  `age` varchar(6) DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gamer
-- ----------------------------
INSERT INTO `gamer` VALUES ('1', '张三', '123456', '男', '20');
INSERT INTO `gamer` VALUES ('2', '李四', '123456', '女', '20');
