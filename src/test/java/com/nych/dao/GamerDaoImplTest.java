package com.nych.dao;

import com.nych.entity.Gamer;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class GamerDaoImplTest{
    private GamerDao gamerDao = new GamerDaoImpl();
    // 添加测试
    @Test
    public void testAdd() throws SQLException {
        Gamer gamer = new Gamer("测试","123456","男","22");
        int i = gamerDao.addGamer(gamer);
        if (i > 0){
            System.out.println("成功添加 " + i + " 个！");
        }else {
            System.out.println("添加玩家失败");
        }
    }
    //删除测试
    @Test
    public void testDelete() throws Exception{
        int i = gamerDao.deleteGamerById(8);
        if (i > 0){
            System.out.println("成功删除了 " + i + " 个玩家！");
        }else {
            System.out.println("删除玩家失败，该玩家不存在！");
        }
    }

    //修改测试
    @Test
    public void testUpdate() throws Exception{
        Gamer gamer = new Gamer(6,"测试","123456","男","22");
        int i = gamerDao.updateGamer(gamer);
        if (i > 0){
            System.out.println("信息修改成功！");
        }else {
            System.out.println("修改失败！");
        }

    }
    //根据id查询
    @Test
    public void testSelectOne() throws Exception{
        Gamer gamer = gamerDao.selectOneGamer(6);
        System.out.println(gamer.toString());
    }
    //查询全部
    @Test
    public void testSelectAll() throws Exception{
        List<Gamer> gamerList = gamerDao.selectAllGamer();
        for (Gamer gamer : gamerList) {
            System.out.println(gamer.toString());
        }
    }

}