
import com.nych.entity.Gamer;
import com.nych.utils.DbUtil;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyTest {

    //新增玩家
    @Test
    public void addGamer() throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "insert into gamer (loginname,`password`,sex,age) values (?,?,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1,"王五");
        statement.setString(2,"123456");
        statement.setString(3,"男");
        statement.setString(4,"20");

        int i = statement.executeUpdate();
        if (i > 0){
            System.out.println("新增 " + i + " 名玩家！");
        }else {
            System.out.println("新增玩家失败！");
        }
        statement.close();
        conn.close();

    }

    //删除玩家
    @Test
    public void delGamerById() throws SQLException {

        Connection conn = DbUtil.getConnection();
        String sql = "delete from gamer where id = ?";

        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1,5);
        int i = statement.executeUpdate();
        System.out.println(i);
        if (i > 0){
            System.out.println("成功删除了 " + i + " 个玩家！");
        }else {
            System.out.println("删除玩家失败");
        }
        statement.close();
        conn.close();
    }

}
