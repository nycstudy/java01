package com.nych.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gamer {
    private int id;
    private String loginName;
    private String password;
    private String sex;
    private String age;

    public Gamer(String loginName, String password, String sex, String age) {
        this.loginName = loginName;
        this.password = password;
        this.sex = sex;
        this.age = age;
    }
}
