package com.nych.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Game {
    private int id;
    private int pid;
    private Date playtime;
    private int score;
    private String loginName;
}
