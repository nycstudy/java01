package com.nych.dao;

import com.nych.entity.Gamer;
import com.nych.utils.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class GamerDaoImpl implements GamerDao{
    //增
    public int addGamer(Gamer gamer) throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "insert into gamer (loginname,`password`,sex,age) values (?,?,?,?)";
        //返回执行是否成功
        int i = 0;
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1,gamer.getLoginName());
        statement.setString(2,gamer.getPassword());
        statement.setString(3,gamer.getSex());
        statement.setString(4,gamer.getAge());
        i = statement.executeUpdate();
        statement.close();
        conn.close();
        return i;
    }
    //删
    public int deleteGamerById(int id) throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "delete from gamer where id = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        int i = statement.executeUpdate();
        statement.close();
        conn.close();
        return i;
    }
    //改
    public int updateGamer(Gamer gamer) throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "update gamer set loginName = ?, `password` = ?, sex = ?, age = ? where id = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1,gamer.getLoginName());
        statement.setString(2,gamer.getPassword());
        statement.setString(3,gamer.getSex());
        statement.setString(4,gamer.getAge());
        statement.setInt(5,gamer.getId());
        int i = statement.executeUpdate();
        statement.close();
        conn.close();
        return i;
    }
    //根据id查玩家
    public Gamer selectOneGamer(int id) throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "select * from gamer where id = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        Gamer gamer = new Gamer();
        while (resultSet.next()){
            gamer.setId(resultSet.getInt("id"));
            gamer.setLoginName(resultSet.getString("loginname"));
            gamer.setPassword("*********");
            gamer.setSex(resultSet.getNString("sex"));
            gamer.setAge(resultSet.getString("age"));
        }
        statement.close();
        conn.close();
        return gamer;
    }
    //查询全部玩家
    public List<Gamer> selectAllGamer() throws SQLException {
        Connection conn = DbUtil.getConnection();
        String sql = "select * from gamer";
        PreparedStatement statement = conn.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        List<Gamer> gamerList = new ArrayList<>();
        while (resultSet.next()){
            Gamer gamer = new Gamer();
            gamer.setId(resultSet.getInt("id"));
            gamer.setLoginName(resultSet.getString("loginname"));
            gamer.setPassword("******");
            gamer.setSex(resultSet.getNString("sex"));
            gamer.setAge(resultSet.getString("age"));
            gamerList.add(gamer);
        }
        statement.close();
        conn.close();
        return gamerList;
    }
}
