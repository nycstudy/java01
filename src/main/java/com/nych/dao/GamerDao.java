package com.nych.dao;

import com.nych.entity.Gamer;

import java.sql.SQLException;
import java.util.List;

public interface GamerDao {
    //添加玩家
    int addGamer(Gamer gamer) throws SQLException;
    //删除玩家
    int deleteGamerById(int id) throws SQLException;
    //修改玩家信息
    int updateGamer(Gamer gamer) throws SQLException;
    //根据id查询一个玩家
    Gamer selectOneGamer(int id) throws SQLException;
    //查询所有玩家
    List<Gamer> selectAllGamer() throws SQLException;
}
