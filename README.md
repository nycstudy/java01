# Java项目实训-第一周作业

#### 介绍
java作业

#### 软件架构
Maven
lombok
jdbc
java


#### 安装教程
1.  下载源码
2. 运行sql文件中的代码，在本地创建数据库
3. 修改utils中的用户名密码改为自己的
![说明](https://images.gitee.com/uploads/images/2021/0913/203219_418246f5_9476415.png "屏幕截图.png")


4.  使用idea打开
5.  等待下载pom.xml中的依赖包

#### 使用说明

1. 在test文件下的GamerDaoImplTest类中运行测试
![测试](https://images.gitee.com/uploads/images/2021/0913/202812_cfc10c2e_9476415.png "屏幕截图.png")

#### 参与贡献

1.  nyc

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
